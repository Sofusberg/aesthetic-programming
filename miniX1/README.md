# miniX1 - README

For my miniX1, I have created a RUNME that allows the user to draw on a “windowWidth” to “windowHeight” canvas with a range of random colors and sizes. The program uses a “mouseDragged” function to enable the user to draw lines along the coordinate system. The program then uses buttons to: reset canvas, change the “strokeWeight” and “stroke” color. The reset canvas function uses a “background” function to create a new blank background. The size function changes the “strokeWeight” of the line to a random weight between 10 to 50 and the color function changes the line color to a random value.

![](/miniX1/draw1.png)

This was my first experience with p5.js and it turned out to be very educational. My intention was to create a fun little drawing program and I managed to do that. This is the advantage of p5.js, it is easy to get started with and the reference system provided me with all the necessary functions that i needed. The program didn’t take much time to create and I would argue that the outcome is very functional and straightforward. The process of creating the program was also rather straightforward with a small amount of trial and error. This has motivated me to set the bar at a higher level for miniX2. All-in-all I think my first experience with p5.js was a success and I am looking forward to creating my next program. 

The process of coding is different from writing text in many ways. When you are writing normal text you dont get punished in the same way when making a mistake. When you code every line, bracket, semicolon and so on must be in the right place or else your program will not run. This can be a frustrating process because it leads to a lot of trial and error. There are also some similarities between normal writhing and worthing. Before the computer can understand your code you must learn and implement the rules recuriered. This is like grammar. If you don't know the rules it will be hard for the reader to convey your message.

Thank you for trying my program. I hope you had fun with it! 👌 🎉

https://boutrup98.gitlab.io/aesthetic-programming/miniX1/

![](/miniX1/200.gif)
